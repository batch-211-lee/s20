console.log("Hello World");

//Repetition Control Structure (Loops)
	//Loops are one of the most important feature that programming must have
	//It lets us execute code repeatedly in a pre-set number or myabe forevere

	function greeting(){
		console.log("Hi, Batch 211");
	};

	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();

	//or we can use loops

	// let countNum = 10;

	// while(countNum !==0 ){
	// 	console.log("This is printed in the loop " + countNum);
	// 	greeting()
	// 	countNum--;
	// };

	//While Loop
	/*
		A while loop takes in an expression/condition
		If the condition evaluates to true, the statements inside the code block will be executed
	*/
	/*
		Syntax:
			while(expression/condition){
				statement/code block
				final expression ++/-- (iteration)
			}

			-expression/iteration - this are the unit of code that is being evaluated in our loop
			Loop will run while the condition/expression is true

			-statement/code block - code/instructions that will be executed several times

			-final expression - this indicates how to advance the loop
	*/

	// let count = 5;
	// //while the value of count is not equal to 0
	// while(count !==0){
	// 	//the current value of count is printed out
	// 	console.log("While: " + count);
	// 	//this decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
	// 	count --;
	// };

	// let num = 20;

	// while(num !== 0){
	// 	console.log("While: num " + num);
	// 	num --;
	// };

		// let digit = 5;
		// while(digit !==20){
		// 	console.log("While: Digit " + digit);
		// 	digit ++;
		// };

		// let num1 = 1;
		// while(num1 === 2){
		// 	console.log("While: nume1 " + num1);
		// 	num1 --;
		// };

		//Do While Loop

		/*
			do-while loop works alot like the while loop
			but unlike while loops,do-while loops gurantee that the code will be executed atleast once
		*/
		/*
			Syntax:
				do {
					statement/code block
					final expression ++/--
				}while (expression/condition)
		*/
		/*
			How it works:
			1. The statements in the "do" block executes once.
			2. The message "Do While:" + number will be printed out in the console
			3. After executing once, the while statement will evaluate wheather to run the next iteration of the loop based on the given expression/condition
			4. If the expressio/condition is not true, another iteration of the loop will be executed and will be repeated until the condition is met
			5.If the condition is met, or true, the loop will stop
		*/
		// let number = Number(prompt("Give me a number"));
		// do{
		// 	console.log("Do while: " + number);
		// 	number += 1;
		// }while(number<10);

	//For Loop
	
	/*A For Loop is more flexible than while and do-while loops

	It consist of three parts:
		1. Initialization value that will track the progression of the loop
		2. Expression/condition that will be evaluated which will determine if the loop will run one more time
		3. Final expression indicates how to davance the loop
	*/

	/*
		Syntax:
			for(Initialization; expression/condition; final expression){
				statement
			};
	*/

	// for (let count = 0; count <= 20; count ++){
	// 	console.log("For Loop: " + count);
	// };

	for(let count = 0; count <=20; count ++){
		if(count%2 === 0){
			console.log("For Loop: " + count)
		};
	};

	let myString = "Waldrich Lee";
	//characters in string may be counted using the .length property
	//strings are special compared to other data types
	console.log(myString.length);

	//Accessing elements of a string
	//individual characters of a string may be accessed using its index number
	//the first character in a string correspond with the number 0, the next is 1....

	console.log(myString[2]);//"l"
	console.log(myString[0]);//"W"

	for(let x = 0; x < myString.length; x ++){
		console.log(myString[x])
	};

	let myName = "WalDrICH"
	let myNewName = " "

	for (let i=0; i<myName.length; i++){
		if(myName[i].toLowerCase() == "a"||
			myName[i].toLowerCase() == "e"||
			myName[i].toLowerCase() == "i"||
			myName[i].toLowerCase() == "o"||
			myName[i].toLowerCase() == "u"){
			console.log(3);
		}else{
			console.log(myName[i]);
			myNewName += myName[i];
		}
	}
	console.log(myNewName);

	//Continueand Break Statements
	/*
		The continue statement allows code to go to the next iteration of the loop without finishing the execution of all statements in a code block

		the break statement is used to terminate the current loop once a match has been found
	*/

	for(let count = 0; count <= 20; count ++){
		if(count % 2 === 0){
			console.log("Even Number")
			//tells the console to continue to the next iteration og the loop
			continue;
			//this ignores all elements located after the continue statement
			//console.log("Hello")
		}
		console.log("Continue and Break: " + count);

		if(count>10){
			//number values after 10 will no longer be printed
			//break tells the code to terminate/ stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
			break;
		}
	}

	let name = "Alexandro";
	for(let i = 0; 1 < name.length; i ++){
		console.log(name[i]);

		if(name[i].toLowerCase() === "a"){
			console.log("Continue")
			continue;
		}

		if(name[i].toLowerCase() === "d"){
			break;
		}
	}